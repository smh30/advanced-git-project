package project1;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "UploadServlet")
public class UploadServlet extends HttpServlet {
    
    private File uploadsFolder;
    private File tempFolder;
    
    public void init() throws ServletException {
        super.init();
        
        // get the upload folder, ensure it exists
        this.uploadsFolder = new File(getServletContext().getRealPath("/Uploaded-Photos"));
        if (!uploadsFolder.exists()) {
            uploadsFolder.mkdirs();
        }
        // creat the necessary temp folder
        this.tempFolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in the upload post");
        
        //see slides from web 16
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        factory.setRepository(tempFolder);
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // get the files
        try {
            List<FileItem> fileItems = upload.parseRequest(request);
            // create empty file where the uploaded file will go
            File fullsizeImageFile = null;
            // there might be more than one file coming which is why it's alist
            
            for (FileItem fi : fileItems){
                if (!fi.isFormField()){
                    String fileName = fi.getName();
                    System.out.println(fileName);
                    fullsizeImageFile = new File(uploadsFolder, fileName);
                    // write the file to the fullsizeImageFile
                    fi.write(fullsizeImageFile);
                }
                out.println("folder: " + fullsizeImageFile.toString());
                out.println("<img src=../Uploaded-Photos/" + fullsizeImageFile.getName() + " " +
                        "width=\"300\">");
            
            }
        } catch (Exception e){
            throw new ServletException(e);
        }
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }
}
