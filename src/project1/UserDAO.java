package project1;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class UserDAO {
    Properties dbProps;
    List<User> allusers;

    public UserDAO(ServletContext context){
        dbProps = new Properties();

        System.out.println("in the UserDAO");

        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e){
            e.printStackTrace();
        }

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/db.properties"))) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //not sure if this method is needed or not, just leaving blank for now
    public List<User> allUsers(){
        // return all of the users
        return allusers;
    }

    //to add a new user into the db - doesn't have any name, etc at this point
    public void newUser(){
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO users default VALUES")) {
                stmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /** checks whether or not the given logincode exists in the database */
    public boolean checkLoginCode(int code){

        System.out.println("checking login colde");
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users WHERE login_code = ?")) {
                stmt.setInt(1, code);
                try (ResultSet rs = stmt.executeQuery()){
                    if (rs.next()){
                        System.out.println("code exists!!");
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("user code does not exist");
        return false;
    }
}
