package beans;

import java.io.Serializable;

public class Entries implements Serializable {
    int entryId;
    int userId;
    String content;
    String timestamp;

    public int getEntry_id() {
        return entryId;
    }

    public void setEntryId(int entry_id) {
        this.entryId = entry_id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int user_id) {
        this.userId = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Entries() {

    }
}