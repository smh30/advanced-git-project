package beans;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EntriesDAO {
    Properties dbProps;
    List <Entries> allentries;

    public EntriesDAO (ServletContext context) {
        dbProps = new Properties();

        try{
            Class.forName("com.mysql.jbdc.Driver");
            //Here we are connecting with sql database
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try(FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/properies"))) {
            dbProps.load(fIn);
            //properties object loading from FileInputStream
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Entries> allEntries() {
        return allentries;
        //Following ppt on this method/relevant lab
        //Will need to assess functionality
    }

//Now that I have connected with SQL database, would like to get information from tables

    public void newEntry() {
        try(Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try(PreparedStatement ps = conn.prepareStatement("INSERT INTO entries default values")) {
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Now I have access to the database and entries table, I need to grab the content data to display entries

    public List<Entries> getUserContent(int userId) {
        List<Entries> entries = new ArrayList <>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM entries WHERE user_id = ?;")) {
                stmt.setInt(1, userId);

                try(ResultSet r = stmt.executeQuery()) {
                    //ResultSets are important as return data from database
                    while (r.next()) {
                        Entries e = new Entries();

                        e.setEntryId(r.getInt(1));
                        e.setUserId(r.getInt(2));
                        e.setContent(r.getString(3));
                        e.setTimestamp(r.getString(4));

                        entries.add(e);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entries;
    }
}