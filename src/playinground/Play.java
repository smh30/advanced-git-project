package playinground;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Play {
    public static void main(String[] args) {
        LocalDateTime timeNow = LocalDateTime.now();
        String stringNow = timeNow.toString();
        System.out.println("here now = " + stringNow);
        
        ZonedDateTime a = timeNow.atZone(ZoneId.systemDefault());
        String stringA = a.toString();
        System.out.println("local zone now =" + stringA );
        
        ZonedDateTime b = a.withZoneSameInstant(ZoneId.of("UTC"));
        System.out.println("utc time = " +b);
        
        String savetime = b.toString();
        System.out.println("this would go in db: " + savetime);
        
        //to get it back
        
        ZonedDateTime c = ZonedDateTime.parse(savetime);
        System.out.println("c = " + c);
    }
}
