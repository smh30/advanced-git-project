ALTER TABLE entries DROP FOREIGN KEY entries_ibfk_1;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
login_code int NOT NULL AUTO_INCREMENT,
name varchar (50),
location varchar (50),
picture varchar (50),
PRIMARY KEY (login_code)
);

INSERT INTO users (name, location, picture) VALUES ('a', 'nz', 'penguin'),
                                                   ('b', 'aus', 'lion'),
                                                   ('c', 'nz', 'seal'),
                                                   ('d', 'gb', 'clown'),
                                                   ('e', 'aus', 'panda');



DROP TABLE IF EXISTS entries;

CREATE TABLE entries (
entry_id int NOT NULL AUTO_INCREMENT,
user_id int,
content varchar (80),
timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(entry_id),
FOREIGN KEY(user_id) REFERENCES users(login_code)
);

INSERT INTO entries (user_id, content) VALUES ('1', 'Puppies'),
                                              ('2', 'Ice cream'),
                                              ('4', 'Chewing gum'),
                                              ('4', 'Steph'),
                                              ('4', 'COFFEE'),
                                              ('3', 'Youtube'),
                                              ('5', 'Sleep');
