<%--
  Created by IntelliJ IDEA.
  User: stephaniehope
  Date: 30/01/19
  Time: 8:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ckedit testing</title>
    <script src="../ckeditor/ckeditor.js"></script>
</head>
<body>
<form action="/ck" method="post">
    <label for="editor">editor</label>
    <textarea name="content" id="editor"></textarea>
    <input type="submit" value="submit the ck">
</form>

<script>
    CKEDITOR.replace('content');
</script>

</body>
</html>
